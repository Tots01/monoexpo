﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using Monoexpo.Engine.Components;

namespace Monoexpo.Engine.Entities
{
    /// <summary>
    /// The Entity abstract general class.
    /// </summary>
    public abstract class Entity
    {
        private List<Component> Components;

        public Entity()
        {
            Components = new List<Component>();
        }

        /// <summary>
        /// Instantiates a component class and adds it to the Components list inside the Entity.
        /// </summary>
        /// <typeparam name="T">The component class type to instantiate and add to the components list.</typeparam>
        /// <param name="aditionalParamaters">The aditional parameters that are probbably required by the component.</param>
        /// <param name="expectedParameters">The expected parameters can be used to find the constructor when you have optional parameters inside the constructors of the component.</param>
        public void AddComponent<T>(object[] aditionalParamaters, Type[] expectedParameters = null) where T : Component
        {
            if (GetComponent<T>() != null)
                throw new Exception("There's already a component with the type \"" + typeof(T).Name + "\" inside this entity!");

            List<object> Parameters = new List<object>
            {
                this
            };
            Parameters.AddRange(aditionalParamaters);

            Component Component = null;

            if (expectedParameters == null)
                Component = (T)Activator.CreateInstance(typeof(T), Parameters.ToArray());
            else
                Component = (T)typeof(T).GetConstructor(expectedParameters).Invoke(Parameters.ToArray());

            Components.Add(Component);
        }

        /// <summary>
        /// Sets a property inside a component.
        /// </summary>
        /// <typeparam name="T">The component class type.</typeparam>
        /// <param name="property">The property name to set.</param>
        /// <param name="value">The value to set the property to.</param>
        public void SetComponentProperty<T>(string property, object value) where T : Component
        {
            int ComponentIndex = Components.IndexOf(GetComponent<T>());

            Util.SetPropertyTo((T)Components[ComponentIndex], property, value);
        }

        /// <summary>
        /// Gets the component based on its class type and returns it.
        /// </summary>
        /// <typeparam name="T">The component class type.</typeparam>
        /// <returns>The component.</returns>
        public T GetComponent<T>() where T : Component
        {
            return (T)Components.Find(c => c.GetType().Name == typeof(T).Name);
        }

        public virtual void Initialize()
        {
            foreach (Component Component in Components)
                Component.Initialize();
        }

        public virtual void LoadContent(ContentManager content)
        {
            foreach (Component Component in Components)
                Component.LoadContent(content);
        }
        
        public virtual void Update(GameTime gameTime)
        {
            foreach (Component Component in Components)
                Component.Update(gameTime);
        }

        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            foreach (Component Component in Components)
                Component.Draw(spriteBatch, gameTime);
        }
    }
}
