﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace Monoexpo.Engine
{
    /// <summary>
    /// This is used to initialize a scene.
    /// </summary>
    public struct SceneConstructor
    {
        public readonly object[] Parameters;
        public readonly Type[] ExpectedParameters;
        public readonly Type SceneClassType;
        
        /// <param name="p">The parameter values.</param>
        /// <param name="sct">The Scene class Type.</param>
        /// <param name="ep">This can be used to express all the parameters of the constructor in order, this will be used to find the constructor.</param>
        public SceneConstructor(object[] p, Type sct, Type[] ep = null)
        {
            Parameters = p;
            ExpectedParameters = ep;
            SceneClassType = sct;
        }
    }

    /// <summary>
    /// This is used to manage all the scenes and the Current Game Scene.
    /// </summary>
    public class SceneManager
    {
        private static SceneManager instance;
        public static SceneManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new SceneManager();
                return instance;
            }
        }

        public List<Scene> Scenes { get; private set; }
        public Scene CurrentScene { get; private set; }

        private SceneManager() { }

        /// <summary>
        /// Initializes the Scenes list and sets the Current Scene.
        /// </summary>
        /// <param name="content">The game content manager.</param>
        /// <param name="graphics">The game graphics device manager.</param>
        /// <param name="sceneConstructors">The scene constructors to intialize all the scenes.</param>
        public void Initialize(ContentManager content, GraphicsDeviceManager graphics, SceneConstructor[] sceneConstructors)
        {
            Scenes = new List<Scene>();

            foreach (SceneConstructor SceneConstructor in sceneConstructors)
            {
                Scene scene = null;

                var Parameters = new List<object>();
                Parameters.AddRange(new object[] { graphics, content });
                Parameters.AddRange(SceneConstructor.Parameters);

                if (SceneConstructor.ExpectedParameters == null)
                    scene = (Scene)Activator.CreateInstance(SceneConstructor.SceneClassType, Parameters.ToArray());
                else
                    scene = (Scene)typeof(SceneConstructor).GetConstructor(SceneConstructor.ExpectedParameters).Invoke(Parameters.ToArray());

                Scenes.Add(scene);
            }

            foreach (Scene Scene in Scenes)
                Scene.Initialize();

            CurrentScene = Scenes[0];
        }
        
        /// <summary>
        /// Sets the current scene to another scene by its type.
        /// </summary>
        /// <typeparam name="T">The Scene class Type.</typeparam>
        public void SetCurrentScene<T>()
        {
            CurrentScene = Scenes.Find(s => s.GetType() == typeof(T));
            CurrentScene.UnloadContent();
        }

        public void LoadContent()
        {
            foreach (Scene Scene in Scenes)
                Scene.LoadContent();
        }

        public void UnloadContent()
        {
            foreach (Scene Scene in Scenes)
                Scene.UnloadContent();
        }

        public void Update(GameTime gameTime)
        {
            CurrentScene.Update(gameTime);
        }

        public void Draw(GameTime gameTime)
        {
            CurrentScene.Draw(gameTime);
        }
    }
}
